from spec_im import SpectralImage
import numpy as np


class PLSpectralImage(SpectralImage):
    """
    Container for ScopeFoundry PL spectral images.

    Container for loading and visualizing CL spectral images. Directly
    indexable by spectral x values. Convenience functions for visualizing
    CL spectral images and spectra. Capable of handling both 2D and 3D
    spectral images.

    Attributes
    ----------
        file_types : list
            List of supported file file_types
        dat : DictionaryTreeBrowser
            metadata dictionary extracted from hdf tree
        spec_im : numpy.ndarray
            Spectral image array with shape
            (num z values, num y values, num x values, num spectral values)
        spec_x_array : numpy.array
            Spectral x array
        spec_units : str
            Spectral units
        x_array : numpy.array
            Array of spatial x values
        y_array : numpy.array
            Array of spatial y values
        z_array : numpy.array
            Array of spatial z values
        units : str
            Spatial units

    Methods
    ----------
        copy(signal=None)
            Return a copy of the SpectralImage. If signal is a
            hyperspy.Signal1D, the spectral image is overwritten with the data
            in signal.
        load_h5_data(fname)
            Load the data from a given hdf5 file name
        load_from_metadata()
            Placeholder for subclasses to implement. Needs to assign values to
            all attributes except file_types and dat
        plot()
            Plots a map of the spectral image summed along the spectral
            direction
        set_background()
        remove_background()
            Removes background correction from data
        to_energy()
            Converts and rebins from wavelength to energy. Returns a new
            SpectralImage or subclass with the same metadata.
        to_index()
            Converts spectral x array to indices
        to_signal()
            Returns a hyperspy.Signal1D with of the spectral image
        plot_spec()
            Plots a spectrum summed over all spatial axes
        get_spec()
        is_supported(fname)
            Checks to see if the string fname is in the list of supported file
            types
    """
    file_types = ['oo_asi_hyperspec_scan.h5', 'asi_OO_hyperspec_scan.h5',
                  'andor_asi_hyperspec_scan.h5', 'oo_asi_hyperspec_3d_scan.h5']

    def load_from_metadata(self):
        try:
            dat = self.dat
            for meas in self.file_types:
                meas = meas[:-3]
                if meas in list(dat['measurement'].keys()):
                    self.name = str(dat['app']['settings']['sample'])
                    # print(self.name)
                    M = dat['measurement'][meas]
                    self.spec_x_array = np.array(M['wls'])
                    self.spec_im = np.array(M['spec_map'])
                    if len(self.spec_im.shape) == 3:
                        self.spec_im = np.reshape(
                            self.spec_im, (1,) + self.spec_im.shape)
                    # print(self.spec_im.shape, M['spec_map'].shape)
                    self.x_array = np.array(M['h_array'])
                    self.y_array = np.array(M['v_array'])
                    if 'z_array' in list(M.keys()):
                        self.z_array = np.array(M['z_array'])
                    else:
                        stage = dat['hardware']['asi_stage']['settings']
                        self.z_array = [stage['z_position']]
                    self.spec_units = 'nm'
                    self.units = 'mm'

                    # print('Sample: ' + self.name)
                    map_min = np.amin(self.spec_im)
                    if map_min < 0:
                        self.spec_im += 1e-2 - map_min
                    # print('%d x %d spatial x %d spectral points' % (
                    #     len(self.x_array), len(self.y_array),
                    #     len(self.spec_x_array)))
        except Exception as ex:
            print('Error loading from dictionary', ex)
