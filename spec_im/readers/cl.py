from spec_im import Image, SpectralImage
import numpy as np
import re


class CLImage(Image):
    file_types = ['sync_raster_scan.h5', 'hyperspec_cl.h5']

    def load_from_metadata(self):
        dat = self.dat
        for meas in self.file_types:
            meas = meas[:-3]
            if meas in list(dat['measurement'].keys()):
                M = dat['measurement'][meas]
                self.description = M['settings']['description']
                hspan = M['settings']['h_span']
                vspan = M['settings']['v_span']
                self.adc_map = np.squeeze(M['adc_map']).copy()
                self.ctr_map = np.squeeze(M['ctr_map']).copy()
                ny, nx, nm = self.adc_map.shape
                H = dat['hardware']
                mag = H['sem_remcon']['settings']['magnification']
                srd = H['sync_raster_daq']['settings']
                whitelist = r'[^a-zA-Z0-9 ]+'
                self.adc_names = re.sub(whitelist, '',
                                        srd['adc_chan_names']).split()
                self.ctr_names = re.sub(whitelist, '',
                                        srd['ctr_chan_names']).split()
                frame_size = 114e-3/mag
                hspan = hspan/20.0*frame_size
                vspan = vspan/20.0*frame_size
                self.x_array = np.linspace(0.0, hspan, num=nx)
                self.y_array = np.linspace(0.0, vspan, num=ny)
                self.units = 'm'
                self.spec_units = 'nm'

    def get_adc(self, name):
        assert name in self.adc_names
        return self.adc_map[:, :, self.adc_names.index(name)]

    def get_ctr(self, name):
        assert name in self.ctr_names
        return self.ctr_map[:, :, self.ctr_names.index(name)]

    def plot_adc(self, name, percentile=5, cmap='viridis', **kwargs):
        adc_map = self.get_adc(name)
        return self._plot(adc_map, percentile=percentile, cmap=cmap, **kwargs)

    def plot_ctr(self, name, percentile=5, cmap='viridis', **kwargs):
        ctr_map = self.get_ctr(name)
        return self._plot(ctr_map, percentile=percentile, cmap=cmap, **kwargs)


class CLSpectralImage(SpectralImage, CLImage):
    """
    Container for ScopeFoundry CL spectral images.

    Container for loading and visualizing CL spectral images. Directly
    indexable by spectral x values. Convenience functions for visualizing
    CL spectral images and spectra.

    Attributes
    ----------
        file_types : list
            List of supported file file_types
        dat : DictionaryTreeBrowser
            metadata dictionary extracted from hdf tree
        spec_im : numpy.ndarray
            Spectral image array with shape
            (num z values, num y values, num x values, num spectral values)
        spec_x_array : numpy.array
            Spectral x array
        spec_units : str
            Spectral units
        x_array : numpy.array
            Array of spatial x values
        y_array : numpy.array
            Array of spatial y values
        z_array : numpy.array
            Array of spatial z values
        units : str
            Spatial units

    Methods
    ----------
        copy(signal=None)
            Return a copy of the SpectralImage. If signal is a
            hyperspy.Signal1D, the spectral image is overwritten with the data
            in signal.
        load_h5_data(fname)
            Load the data from a given hdf5 file name
        load_from_metadata()
            Placeholder for subclasses to implement. Needs to assign values to
            all attributes except file_types and dat
        plot()
            Plots a map of the spectral image summed along the spectral
            direction
        set_background()
        remove_background()
            Removes background correction from data
        to_energy()
            Converts and rebins from wavelength to energy. Returns a new
            SpectralImage or subclass with the same metadata.
        to_index()
            Converts spectral x array to indices
        to_signal()
            Returns a hyperspy.Signal1D with of the spectral image
        plot_spec()
            Plots a spectrum summed over all spatial axes
        get_spec()
        is_supported(fname)
            Checks to see if the string fname is in the list of supported file
            types
    """
    file_types = ["hyperspec_cl.h5", ]

    def __init__(self, fname='', dat=None):
        SpectralImage.__init__(self, fname=fname, dat=dat)

    def load_from_metadata(self):
        CLImage.load_from_metadata(self)
        M = self.dat['measurement'][self.file_types[0][:-3]]
        self.spec_im = np.squeeze(M['spec_map'])
        self.spec_im = np.reshape(self.spec_im, (1,) + self.spec_im.shape)
        self.spec_x_array = np.array(M['wls'])
        self.z_array = [0.0]
